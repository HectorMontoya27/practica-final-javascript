$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		$.ajax({
			url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=1231231231564464541231&query="+palabra,
			success: function(respuesta){
				console.log(respuesta);
				cardHTML = crearMovieCard(respuesta)
				miLista.append('<li>'+cardHTML+'</li>')
			},
			error: function() {
				console.log("No se ha podido obtener la información");
			},
			beforeSend: function() {
				console.log('CARGANDO');
				$('#peliculas').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
			},
		})
	});

});

function crearMovieCard(movie){
	//Llega el objeto JSON de UNA película, como la regresa la API
	console.log(movie.poster_path);
    //sabemos que el directorio donde se guardan las imágenes es: https://image.tmdb.org/t/p/w500/
    //el atributo movie.poster_path del objeto movie, sólo contiene el nombre de la imagen (NO la ruta completa)

    //NOTAR que se accede al objeto JSON movie con la notación de punto para acceder a los atributos (movie.original_title).
	var cardHTML =
		'<!-- CARD -->'
		+'<div class="col-md-4">'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+movie.original_title+'</h2>'
		          +'<div class="container">'
		             +'<div class="row">'
		                +'<div class="col-4 metadata">'
		                   +'<i class="fa fa-star" aria-hidden="true"></i>'
		                   +'<p>9.5/10</p>'
		                +'</div>'
		                +'<div class="col-8 metadata">Adventure. Sci-Fi</div>'
		             +'</div>'
		          +'</div>'
		          +'<p class="card-text">A team of explorers travel through wormhole in space in an attempt to ensure humanitys survival.</p>'
		       +'</div>'
		    +'</div>'
		+'</div>'
		+'<!-- CARD -->';

		return cardHTML;
}